({
    doInit : function(component, event, helper) {
        helper.fetchContractClauses(component, event, helper);
    },       
    handleComponentEvent  : function(component, event, helper)
    {
        helper.deleteCCmethod(component, event, helper);
        
        helper.fetchContractClauses(component, event, helper);
    },    
    createCCRecord : function(component, event, helper)
    {
        helper.createRec(component, event, helper);                 
    }    
})