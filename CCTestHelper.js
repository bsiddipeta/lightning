({
    fetchContractClauses : function(component, event, helper) {       
        //  alert('inside helper');
        var action = component.get('c.getAllContractClauses');
        action.setParams({
            "conid" : component.get("v.recordId"),            
        });       
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS')
            {
                // alert(state);
                component.set("v.CCRelatedList", response.getReturnValue() );
                console.log(component.get("v.CCRelatedList"));}
            
        } , 'ALL');
        $A.enqueueAction(action);
    },
    
    deleteCCmethod :function(component, event, helper) {              
        //   alert('inside delete');
        var delrecId = event.getParam("deleteCCrecord");
        component.set('v.toDeleteRec', delrecId);       
        var action = component.get('c.deleteccRecord');
        action.setParams({            
            "ccIds" : delrecId, 
        });       
        action.setCallback( this, function(response){
            var state = response.getState();                       
        } , 'ALL');               
        $A.enqueueAction(action);
    },
    createRec : function(component, event, helper){
         //alert('inside new cc');
        var createRecordEvent = $A.get('e.force:createRecord');
        var LOOKUP = 'LOOKUP'; 
        createRecordEvent.setParams({            
            "entityApiName" : "Contract_Clause__c",  
            "navigationLocation":LOOKUP,
            "panelOnDestroyCallback": function() {
                helper.fetchContractClauses(component, event, helper);
            },
            "defaultFieldValues": {
                'Contract__c' : component.get('v.recordId'),        
            }           
        });           
        createRecordEvent.fire(); 
    }
})